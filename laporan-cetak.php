<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body onload="print()">

	<?php
include "includes/config.php";
$config = new Config();
$db = $config->getConnection();
include_once 'includes/alternatif.inc.php';
$pro1 = new Alternatif($db);
$stmt1 = $pro1->readAll();
$stmt1y = $pro1->readAll();
include_once 'includes/kriteria.inc.php';
$pro2 = new Kriteria($db);
$stmt2 = $pro2->readAll();
$stmt2y = $pro2->readAll();
include_once 'includes/rangking.inc.php';
$pro = new Rangking($db);
$stmt = $pro->readKhusus();
$stmty = $pro->readKhusus();
$count = $pro->countAll();
$stmtx1 = $pro->readBob();
$stmtx2 = $pro->readBob();
$stmtx1y = $pro->readBob();
$stmtx2y = $pro->readBob();

function get_data($tabel,$primary_key,$id,$select,$koneksi)
{
	$sql = "SELECT $select FROM $tabel where $primary_key='$id'";
    $result = mysqli_query($koneksi, $sql);
	$row = mysqli_fetch_assoc($result);
	return $row[$select];
}

?>

			<h3>Hasil Perangkingan MPE</h3>
			<br>
			<table width="100%" class="table table-striped table-bordered">
		        <thead>
		            <tr>
		                <th rowspan="2" style="vertical-align: middle" class="text-center">ID Alternatif</th>
		                <th rowspan="2" style="vertical-align: middle" class="text-center">Nama</th>
		                <th colspan="<?php echo $stmt2->rowCount(); ?>" class="text-center">Kriteria</th>
		                <th rowspan="2" style="vertical-align: middle" class="text-center">Total Nilai</th>
		                <th rowspan="2" style="vertical-align: middle" class="text-center">Ranking</th>
		            </tr>
		            <tr>
		            <?php
					while ($row2 = $stmt2y->fetch(PDO::FETCH_ASSOC)){
					?>
		                <th><?php echo $row2['nama_kriteria'] ?></th>
		            <?php
					}
					?>
		            </tr>
		        </thead>
		
		        <tbody>
		<?php
		$no=1;
		while ($row1 = $stmt1y->fetch(PDO::FETCH_ASSOC)){
		?>
		            <tr>
		                <th><?php echo $row1['id_alternatif'] ?></th>
		                <th><?php echo $row1['nama_alternatif'] ?></th>
		                <?php
		                $a1= $row1['id_alternatif'];
						$stmt21 = $pro2->readAll();
						while ($row21 = $stmt21->fetch(PDO::FETCH_ASSOC)){
							$b2= $row21['id_kriteria'];
							$stmtr = $pro->readR($a1,$b2);
							while ($rowr = $stmtr->fetch(PDO::FETCH_ASSOC)){
						?>
		                <td>
		                	<?php 
		                	// perhitungan AHP
		                	// echo $norx = $rowr['skor_alt_kri']*$row21['bobot_kriteria'];

		                	// perhitungan MPE
		                	echo $norx = pow($rowr['jumlah_alt_kri'],$row21['bobot_kriteria']);
							//pow($rowr['skor_alt_kri'],$bobot);
							$pro->ia = $a1;
							$pro->ik = $b2;
							$pro->nn4 = $norx;
							$pro->normalisasi1();
		                	?>
		                </td>
		                <?php
							}
		                }
						?>
						<td>
							<?php 
							$stmthasil = $pro->readHasil1($a1);
							$hasil = $stmthasil->fetch(PDO::FETCH_ASSOC);
							echo $hasil['bbn'];
							$pro->ia = $a1;
							$pro->has1 = $hasil['bbn'];
							$pro->hasil1();
							?>
						</td>
						<td>
							<?php echo $no++; ?>
						</td>
		            </tr>
		<?php
		}
		?>
				<!-- <tr>
					<th>Jumlah</th>
					<?php
					while ($rowx2 = $stmtx2y->fetch(PDO::FETCH_ASSOC)){
					?>
		                <td>
						<?php 
						$stmtx3y = $pro->readMax($rowx2['id_kriteria']);
						$rowx3 = $stmtx3y->fetch(PDO::FETCH_ASSOC);
						echo number_format($rowx3['mnr1'], 5, '.', ',');
						?>
						</td>
		            <?php
					}
					?>
					<td>
					<?php 
						$stmtx4y = $pro->readMax2();
						$rowx4 = $stmtx4y->fetch(PDO::FETCH_ASSOC);
						echo number_format($rowx4['mnr2'], 5, '.', ',');
					?>
					</td>
				</tr> -->
		        </tbody>
		
		    </table>


</body>
</html>