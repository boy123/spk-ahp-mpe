<?php
include_once 'header.php';
include_once 'includes/kriteria.inc.php';
$pro1 = new Kriteria($db);
$count1 = $pro1->countAll();
include_once 'includes/nilai.inc.php';
$pro2 = new Nilai($db);

function get_data($tabel,$primary_key,$id,$select,$koneksi)
{
	$sql = "SELECT $select FROM $tabel where $primary_key='$id'";
    $result = mysqli_query($koneksi, $sql);
	$row = mysqli_fetch_assoc($result);
	return $row[$select];
}
/*if($_POST){
	
	include_once 'includes/bobot.inc.php';
	$eks = new Bobot($db);

	$eks->nm = $_POST['nm'];
	
	if($eks->insert()){
?>
<script type="text/javascript">
window.onload=function(){
	showStickySuccessToast();
};
</script>
<?php
	}
	
	else{
?>
<script type="text/javascript">
window.onload=function(){
	showStickyErrorToast();
};
</script>
<?php
	}
}*/
?>
		<div class="row">
		  <div class="col-xs-12 col-sm-12 col-md-2">
		  <?php
			include_once 'sidebar.php';
			?>
		  </div>
		  <div class="col-xs-12 col-sm-12 col-md-10">
		  <ol class="breadcrumb">
			  <li><a href="index.php"><span class="fa fa-home"></span> Beranda</a></li>
			  <li class="active"><span class="fa fa-bomb"></span> Analisa Data</li>
	  		  <li><a href="nilai_alternatif.php"><span class="fa fa-table"></span> Nilai Alternatif</a></li>
			</ol>
		  	<p style="margin-bottom:10px;">
		  		<strong style="font-size:18pt;"><span class="fa fa-bomb"></span> Nilai Alternatif</strong>
		  	</p>
		  	<div class="panel panel-default">
		<div class="panel-body">
		
		<table class="table table-striped">
			<tr>
				<th>ID Alternatif</th>
				<th>Nama</th>
				<th>Kriteria</th>
				<th>Kategori Kriteria</th>
				<th>Nilai</th>
				<th>Bobot</th>
				<th>Option</th>
			</tr>

			<?php 
			$sql = 'SELECT * FROM ahp_jum_alt_kri';
         	$result = mysqli_query($koneksi, $sql);
         	if (mysqli_num_rows($result) > 0) {
	            while($row = mysqli_fetch_assoc($result)) {
	               ?>
	               	<tr>
						<td><?php echo $row['id_alternatif'] ?></td>
						<td><?php echo get_data('ahp_data_alternatif','id_alternatif',$row['id_alternatif'],'nama_alternatif',$koneksi) ?></td>
						<td><?php echo get_data('ahp_data_kriteria','id_kriteria',$row['id_kriteria'],'nama_kriteria',$koneksi) ?></td>
						<td>
							<form action="proses_edit_nilai_alternatif.php?id_alt=<?php echo $row['id_alternatif'] ?>&id_kri=<?php echo $row['id_kriteria'] ?>" method="POST">
							<select name="kategori_kriteria" class="form-control">
								<option value="">--Pilih--</option>
								<option value="3">Baik</option>
								<option value="2">Cukup</option>
								<option value="1">Kurang</option>
								<option value="3">IT, Elektro</option>
								<option value="2">MIPA, Fisika</option>
								<option value="1">Jurusan lain</option>
								<option value="3"> < 5 km</option>
								<option value="2"> 5-10 km</option>
								<option value="1"> > 10 km </option>
							</select>
						</td>
						<td>
							<?php echo $row['jumlah_alt_kri'] ?>
						</td>
						<td>
							<?php echo get_data('ahp_data_kriteria','id_kriteria',$row['id_kriteria'],'bobot_kriteria',$koneksi) ?>
							<input type="hidden" name="bobot" value="<?php echo get_data('ahp_data_kriteria','id_kriteria',$row['id_kriteria'],'bobot_kriteria',$koneksi) ?>">
						</td>
						<td>
							<button type="submit" class="btn btn-primary btn-sm">Ubah</button>
							</form>
						</td>
					</tr>
	               <?php
	            }
	         } else {
	            	?>

	               <?php
	         }
			 ?>
			
		</table>
			    
			  
		  </div></div></div>
		</div>
		<?php
include_once 'footer.php';
?>